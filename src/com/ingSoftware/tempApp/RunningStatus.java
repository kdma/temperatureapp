package com.ingSoftware.tempApp;

import android.app.Application;

/**
 * Created by Franco on 23/06/2014.
 */
public class RunningStatus  extends Application {

        public static boolean isActivityVisible() {
            return activityVisible;
        }

        public static void activityResumed() {
            activityVisible = true;
        }

        public static void activityPaused() {
            activityVisible = false;
        }

        private static boolean activityVisible;

}

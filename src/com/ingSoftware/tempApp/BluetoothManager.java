package com.ingSoftware.tempApp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Franco on 23/06/2014.
 */
public class BluetoothManager extends Thread {

    private final BluetoothServerSocket _bluetoothSocket;
    private MainThreadBus _bus;

    private final UUID PY_UUID = UUID.fromString("7e6d5fd3-b9e2-4eac-b562-8679691099fc");
    private final String NAME = "TEMPERATURE APP";
    public static final int CONNECTED = 1;
    public static final int CONNECTION_TIMEOUT = 0;
    private final int TIMEOUT = 40000;


    private final String TAG = "BluetoothManager";

    public BluetoothManager(MainThreadBus bus) {
            _bus = bus;
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                // Device does not support Bluetooth
            }
            // Use a temporary object that is later assigned to _bluetoothSocket,
            // because _bluetoothSocket is final
            BluetoothServerSocket tmp = null;
            try {
                // PY_UUID is the app's UUID string, also used by the pyhton client
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME, PY_UUID);
            } catch (IOException e) {
                Log.v(TAG, e.getMessage());
            }
            _bluetoothSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned
            while (true) {
                try {
                    socket = _bluetoothSocket.accept();
                } catch (IOException e) {
                    break;
                }
                // If a connection was accepted
                if (socket != null) {
                    // Do work to manage the connection (in a separate thread)
                    ConnectedThread t = new ConnectedThread(socket);
                    _bus.post(new Integer(CONNECTED));
                    t.run();
                    try {
                        _bluetoothSocket.close();
                    } catch (Exception e)  {
                        Log.v(TAG, e.getMessage());
                    }
                    break;
                }
            }
        }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket _socket;
        private final InputStream _inputSTream;
        private final String TAG = "Bluetooth Connected";

        public ConnectedThread(BluetoothSocket socket) {
            _socket = socket;
            InputStream tmpIn = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.v(TAG, e.getMessage());
            }

            _inputSTream = tmpIn;
        }

        public void run() {
            int bytes; // bytes returned from read()
            int availableBytes = 0;
            long startTime = System.currentTimeMillis();
            long elapsedTime = 0L;

            // Keep listening to the InputStream until an event occurs
            while (true && elapsedTime <= TIMEOUT) {
                elapsedTime = (new Date()).getTime() - startTime;
                try {
                    availableBytes = _inputSTream.available();
                    if(availableBytes > 0){
                        startTime = System.currentTimeMillis();
                        elapsedTime = 0;
                        byte[] buffer = new byte[availableBytes];
                        bytes = _inputSTream.read(buffer);
                        // Send the obtained bytes to the temp monitor activity
                        String value = new String(buffer);
                        Log.v(TAG, "received temp " + value);
                        TempData tempData = new TempData(Double.parseDouble(value));
                        _bus.post(tempData);
                    }
                } catch (IOException e) {
                    Log.v(TAG, e.getMessage());
                    break;
                }
            }
            _bus.post(CONNECTION_TIMEOUT);
            Log.v(TAG, "Connection timed out");

        }
    }
}

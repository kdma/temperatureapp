package com.ingSoftware.tempApp;

/**
 * Created by kdma on 24/06/14.
 */
public interface ISensor {
    public Double getValue();
}

package com.ingSoftware.tempApp;

/**
 * Created by kdma on 22/06/14.
 */
public class TempData implements ISensor {

    private Double _temp;
    public TempData(Double temp){
        _temp = temp;
    }

    public Double getValue(){
        return _temp;
    }
}

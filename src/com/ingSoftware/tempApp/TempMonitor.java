package com.ingSoftware.tempApp;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.jjoe64.graphview.*;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.squareup.otto.Subscribe;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kdma on 19/06/14.
 */
public class TempMonitor extends Activity {
    private GraphView _graphView;
    private GraphViewSeries _temperatureSeries;
    private GraphViewData[] _tempPoints;

    private final int SIZE = 40;
    private final double HIGH_TEMP = 23;
    private final int HIGH_BOUND = 30;
    private final int LOW_BOUND = 8;
    private final int MID_TEMP = 16;
    private BluetoothManager _btListener;
    private NotificationManager _notificationManager;
    private AlertDialog _alertDialog;

    @Override
    public void onPause(){
        super.onPause();
        RunningStatus.activityPaused();
    }
    @Override
    public void onResume(){
        super.onResume();
        RunningStatus.activityResumed();
    }
    @Override
    public void onBackPressed(){
        RunningStatus.activityPaused();
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        RunningStatus.activityPaused();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RunningStatus.activityResumed();

        MainThreadBus mainThreadBus = new MainThreadBus();
        mainThreadBus.register(this);
        setContentView(R.layout.graph);
        SetupGraph();

        _btListener = new BluetoothManager(mainThreadBus);
        _btListener.start();

        _notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    }

    @Subscribe
    public void onSensorReadingReceived(TempData value) {
        // Receive message from bluetooth
        long now = new Date().getTime();
        double temperature = value.getValue();
        GraphViewData data = new GraphViewData(now,temperature) ;
        _temperatureSeries.appendData(data, false, SIZE);
        _graphView.redrawAll();
        if(temperature >= HIGH_TEMP){
            NotifyHighTemp(temperature);
        }
        TextView counter = (TextView)findViewById(R.id.temp);
        counter.setText("Received temp: " + Math.round(temperature) + "°");
    }

    @Subscribe
    public void onConnectionStatus(Integer status){
        //Received connection from bluetooth
        if(status == BluetoothManager.CONNECTED){
            Toast.makeText(getBaseContext(),"Bluetooth client connected", Toast.LENGTH_SHORT).show();
        }else if(status == BluetoothManager.CONNECTION_TIMEOUT){
            if(RunningStatus.isActivityVisible() == true){
                Toast.makeText(getBaseContext(),"Bluetooth client timed out", Toast.LENGTH_SHORT).show();
                finish();
            }else{
               BuildNotification("Connection Error", "Bluetooth timed out");
            }
        }
    }

    public void NotifyHighTemp(double temperature){
        if(RunningStatus.isActivityVisible()){
            ShowTempDialog(Math.round(temperature));
        }else{
            CreateNotification(temperature);
        }
    }

    private void SetupGraph(){
        _tempPoints = new GraphViewData[SIZE];
        FillWithFakeData();
        SetGraphStyle();
    }

    private void FillWithFakeData(){
        long now = new Date().getTime();
        for (int i = 0;i<SIZE;i++){
            _tempPoints[i] = new GraphViewData(now,MID_TEMP);
        }
    }

    private void SetGraphStyle(){
        _graphView = new LineGraphView(
                this // context
                , "Temperatura " // heading
        );
        _graphView.setManualYAxis(true);
        _graphView.setManualYAxisBounds(HIGH_BOUND, LOW_BOUND);
        _temperatureSeries = new GraphViewSeries(_tempPoints);
        _graphView.addSeries(_temperatureSeries); // _tempPoints

        final SimpleDateFormat dateFormat = new SimpleDateFormat("HH.mm.ss");
        //set il tempo come asse-x
        _graphView.setCustomLabelFormatter(new CustomLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    Date d = new Date((long) value);
                    return dateFormat.format(d);
                } else {
                    //asse y automatico
                    return null;
                }
            }
        });

        //stile
        ((LineGraphView) _graphView).setDrawBackground(true);
        _graphView.getGraphViewStyle().setVerticalLabelsWidth(60);
        _graphView.getGraphViewStyle().setNumVerticalLabels(12);
        _graphView.getGraphViewStyle().setNumHorizontalLabels(4);

        LinearLayout layout = (LinearLayout) findViewById(R.id.graph);
        layout.addView(_graphView);
    }


    private void AlertSound()  {
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        toneG.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT, 300);
    }

    private void ShowTempDialog(double temp){
        _alertDialog = new AlertDialog.Builder(this).setTitle("Temperatura Elevata").setMessage("La temperatura nell'abitacolo è di " + temp + "°").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create();
        _alertDialog.show();
        AlertSound();
    }

    private void CreateNotification(double temp) {
        String title = "Attenzione!";
        String text  = "La temperatura nell abitacolo è di " + temp + "°";
        Intent showGraph = new Intent(getApplicationContext(), TempMonitor.class);
        PendingIntent StartIntent = PendingIntent.getActivity(getApplicationContext(), 0, showGraph, PendingIntent.FLAG_CANCEL_CURRENT);
        BuildNotification(title,text,StartIntent);
    }

    private void BuildNotification(String title, String text){
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .build();
        ShowNotification(notification);
    }

    private void BuildNotification(String title, String text, PendingIntent startIntent){

        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setContentIntent(startIntent)
                .build();
        ShowNotification(notification);
    }

    private void ShowNotification(Notification notification){
        int NOTIFICATION_ID = 1;
        _notificationManager.notify(NOTIFICATION_ID, notification);
        AlertSound();
    }
}





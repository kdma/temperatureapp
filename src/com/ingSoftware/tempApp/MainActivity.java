package com.ingSoftware.tempApp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

         findViewById(R.id.btn_graph).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 startActivity(TempMonitor.class);
             }
         });

    }

    private void startActivity(Class<? extends Activity> activity) {
        Intent intent = new Intent(MainActivity.this, activity);
        startActivity(intent);
    }

}
